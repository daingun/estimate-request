# Estimate Request

Web application to manage the estimate requests for customized products.

The application has a thin client, does not use javascript, every computation is done on the server, the template engine is thymeleaf.

The project contains the web application, the database schema and the software design document.

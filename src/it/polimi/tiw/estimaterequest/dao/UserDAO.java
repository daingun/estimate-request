package it.polimi.tiw.estimaterequest.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import it.polimi.tiw.estimaterequest.beans.User;

public class UserDAO {
    /**
     * Database connection.
     */
    private Connection connection;

    /**
     * Create a new object to access the user data from the database.
     * 
     * @param connection - Database connection.
     */
    public UserDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Retrieve the user given the user-name and password.
     * 
     * @param username - user-name.
     * @param password - password.
     * @return An Optional containing the selected user.
     * @throws SQLException - If internal SQL query fails.
     */
    public Optional<User> checkCredentials(String username, String password) throws SQLException {
        String query = "SELECT user_id, username, name, surname, permissions FROM user"
                + " WHERE username = ? AND password = ?";
        try (PreparedStatement ps = connection.prepareStatement(query);) {
            ps.setString(1, username);
            ps.setString(2, password);
            try (ResultSet result = ps.executeQuery();) {
                if (!result.isBeforeFirst()) {
                    return Optional.empty();
                } else {
                    result.next();
                    User user = new User();
                    user.setId(result.getInt("user_id"));
                    user.setUsername(result.getString("username"));
                    user.setName(result.getString("name"));
                    user.setSurname(result.getString("surname"));
                    user.setPermission(result.getString("permissions"));
                    return Optional.of(user);
                }
            }
        }
    }

}

package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import it.polimi.tiw.estimaterequest.beans.FullEstimate;
import it.polimi.tiw.estimaterequest.dao.EstimateDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;
import it.polimi.tiw.estimaterequest.utils.TemplateHandler;

/**
 * Servlet implementation class EstimateDetails
 */
@WebServlet("/EstimateDetails")
public class EstimateDetails extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;
    private TemplateEngine templateEngine;

    public EstimateDetails() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
        templateEngine = TemplateHandler.initTemplateEngine(this);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String loginPath = getServletContext().getContextPath() + "/index.html";
        HttpSession session = request.getSession();
        if (session.isNew() || session.getAttribute("user") == null) {
            response.sendRedirect(loginPath);
            return;
        }

        EstimateDAO estimateDAO = new EstimateDAO(connection);
        FullEstimate estimate;
        try {
            int estimateId = Integer.parseInt(request.getParameter("estId"));
            estimate = estimateDAO.getFullEstimateById(estimateId).get();
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not possible to recover selected product");
            return;
        }

        // Redirect to the page to assign a price to the estimate.
        String employeePath = "/WEB-INF/priceEstimate.html";
        ServletContext servletContext = getServletContext();
        final WebContext ctx = new WebContext(request, response, servletContext,
                request.getLocale());
        ctx.setVariable("estimate", estimate);
        templateEngine.process(employeePath, ctx, response.getWriter());
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

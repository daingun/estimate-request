package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import it.polimi.tiw.estimaterequest.beans.User;
import it.polimi.tiw.estimaterequest.dao.UserDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;
import it.polimi.tiw.estimaterequest.utils.TemplateHandler;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;
    private TemplateEngine templateEngine;

    public Login() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
        templateEngine = TemplateHandler.initTemplateEngine(this);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        if (username == null || password == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing authentication values");
            return;
        }

        UserDAO userDao = new UserDAO(connection);
        Optional<User> user;
        try {
            user = userDao.checkCredentials(username, password);
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not Possible to check credentials");
            return;
        }

        if (user.isPresent()) {
            User u = user.get();
            request.getSession().setAttribute("user", u);
            String path = getServletContext().getContextPath();
            if (u.getPermission().equals("client")) {
                path += "/ClientHome";
            } else if (u.getPermission().equals("employee")) {
                path += "/EmployeeHome";
            }
            response.sendRedirect(path);
        } else {
            ServletContext servletContext = getServletContext();
            final WebContext ctx = new WebContext(request, response, servletContext,
                    request.getLocale());
            ctx.setVariable("errorMsg", "No user with username: " + username);
            String path = "/index.html";
            templateEngine.process(path, ctx, response.getWriter());
        }
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

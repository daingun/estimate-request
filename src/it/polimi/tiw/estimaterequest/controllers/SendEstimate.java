package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import it.polimi.tiw.estimaterequest.beans.User;
import it.polimi.tiw.estimaterequest.dao.EstimateDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;

/**
 * Servlet implementation class SendEstimate
 */
@WebServlet("/SendEstimate")
public class SendEstimate extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;

    public SendEstimate() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String loginPath = getServletContext().getContextPath() + "/index.html";
        HttpSession session = request.getSession();
        if (session.isNew() || session.getAttribute("user") == null) {
            response.sendRedirect(loginPath);
            return;
        }

        // Se non ci sono opzioni ripeti l'inserimento dei dati.
        String[] options = request.getParameterValues("selOpt");
        if (options == null) {
            String path = getServletContext().getContextPath() + "/ClientHome?error=true";
            response.sendRedirect(path);
            return;
        }

        User client = (User) session.getAttribute("user");

        EstimateDAO estimateDAO = new EstimateDAO(connection);
        try {
            int productCode = Integer.parseInt(request.getParameter("prodId"));
            @SuppressWarnings("unused")
            int last_id = estimateDAO.sendEstimate(client.getId(), productCode, options);
        } catch (SQLException | NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not possible to save estimate");
            return;
        }

        String path = getServletContext().getContextPath() + "/ClientHome";
        response.sendRedirect(path);
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

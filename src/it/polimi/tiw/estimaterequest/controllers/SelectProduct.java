package it.polimi.tiw.estimaterequest.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import it.polimi.tiw.estimaterequest.beans.FullEstimate;
import it.polimi.tiw.estimaterequest.beans.Option;
import it.polimi.tiw.estimaterequest.beans.Product;
import it.polimi.tiw.estimaterequest.beans.User;
import it.polimi.tiw.estimaterequest.dao.EstimateDAO;
import it.polimi.tiw.estimaterequest.dao.OptionDAO;
import it.polimi.tiw.estimaterequest.dao.ProductDAO;
import it.polimi.tiw.estimaterequest.utils.ConnectionHandler;
import it.polimi.tiw.estimaterequest.utils.TemplateHandler;

/**
 * Servlet implementation class SelectProduct
 */
@WebServlet("/SelectProduct")
public class SelectProduct extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private Connection connection;
    private TemplateEngine templateEngine;

    public SelectProduct() {
        super();
    }

    @Override
    public void init() throws ServletException {
        connection = ConnectionHandler.getConnection(getServletContext());
        templateEngine = TemplateHandler.initTemplateEngine(this);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String loginPath = getServletContext().getContextPath() + "/index.html";
        HttpSession session = request.getSession();
        if (session.isNew() || session.getAttribute("user") == null) {
            response.sendRedirect(loginPath);
            return;
        }

        User client = (User) session.getAttribute("user");
        EstimateDAO estimateDAO = new EstimateDAO(connection);
        List<FullEstimate> estimates;
        try {
            estimates = estimateDAO.getFullEstimateByClientId(client.getId());
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not possible to recover estimates");
            return;
        }

        ProductDAO productDAO = new ProductDAO(connection);
        Product product = new Product();
        int productCode;
        try {
            productCode = Integer.parseInt(request.getParameter("selectedProduct"));
            product = productDAO.getProductByCode(productCode).get();
        } catch (SQLException | NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not possible to recover selected product");
            return;
        }

        OptionDAO optionDAO = new OptionDAO(connection);
        List<Option> options;
        try {
            options = optionDAO.getProductOptions(productCode);
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    "Not possible to recover selected product");
            return;
        }

        // Redirect to Client Home Page
        String clientPath = "/WEB-INF/client.html";
        ServletContext servletContext = getServletContext();
        final WebContext ctx = new WebContext(request, response, servletContext,
                request.getLocale());
        ctx.setVariable("estimates", estimates);
        ctx.setVariable("product", product);
        ctx.setVariable("options", options);
        ctx.setVariable("productSelected", true);
        templateEngine.process(clientPath, ctx, response.getWriter());
    }

    @Override
    public void destroy() {
        try {
            ConnectionHandler.closeConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

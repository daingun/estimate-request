package it.polimi.tiw.estimaterequest.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.UnavailableException;

public class ConnectionHandler {

    /**
     * Creates the connection to the database.
     * 
     * @param context ServletContext that contains the connection parameters.
     * @return The connection to the database.
     * @throws UnavailableException If the database driver cannot be loaded or the
     *                              connection cannot be created.
     */
    public static Connection getConnection(ServletContext context) throws UnavailableException {
        Connection connection = null;
        try {
            String driver = context.getInitParameter("dbDriver");
            String url = context.getInitParameter("dbUrl");
            String user = context.getInitParameter("dbUser");
            String password = context.getInitParameter("dbPassword");
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            throw new UnavailableException("Can't load database driver: " + e.getCause());
        } catch (SQLException e) {
            throw new UnavailableException("Couldn't get db connection: " + e.getCause());
        }
        return connection;
    }

    /**
     * Closes the database connection.
     * 
     * @param connection - The Connection to be closed.
     * @throws SQLException If a database access error occurs.
     */
    public static void closeConnection(Connection connection) throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }
}

package it.polimi.tiw.estimaterequest.utils;

import javax.servlet.GenericServlet;
import javax.servlet.ServletContext;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

public class TemplateHandler {

    public static TemplateEngine initTemplateEngine(GenericServlet servlet) {
        ServletContext servletContext = servlet.getServletContext();
        ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(
                servletContext);
        templateResolver.setTemplateMode(TemplateMode.HTML);
        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        templateResolver.setSuffix(".html");
        return templateEngine;
    }
}
